import request from '@/utils/request'
import Qs from 'qs'

export function fetchList(query) {
    return request({
        url: '/user/list',
        method: 'get',
        params: query
    })
}

export function fetchArticle(id) {
    return request({
        url: '/article/detail',
        method: 'get',
        params: { id }
    })
}

export function fetchPv(pv) {
    return request({
        url: '/article/pv',
        method: 'get',
        params: { pv }
    })
}

export function createUser(data) {
    const data1 = data
    return request({
        url: '/user/create',
        method: 'post',
        data: Qs.stringify(data1)
    })
}

export function deleteUser(uid) {
    return request({
        url: '/user/delete',
        method: 'delete',
        params: { uid: uid }
    })
}

export function updateUser(data) {
    const data1 = data
    return request({
        url: '/user/update',
        method: 'post',
        data: Qs.stringify(data1)
    })
}

export function updateArticleStatus(id, status) {
    return request({
        url: '/article/updateArticleStatus',
        method: 'post',
        data: Qs.stringify({ id, status })
    })
}