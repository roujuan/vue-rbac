import request from '@/utils/request'
import Qs from 'qs'

export function getPictureList(query) {
    return request({
        url: '/picture/list',
        method: 'get',
    })
}

export function fetchList(query) {
    return request({
        url: '/picture/queryList',
        method: 'get',
        params: query
    })
}

export function deletePicture(id) {
    return request({
        url: '/picture/delete',
        method: 'delete',
        params: { id: id }
    })
}

export function updatePicture(data) {
    const data1 = data
    return request({
        url: '/picture/update',
        method: 'post',
        data: Qs.stringify(data1)
    })
}

export function createPicture(data) {
    const data1 = data
    return request({
        url: '/picture/create',
        method: 'post',
        data: Qs.stringify(data1)
    })
}