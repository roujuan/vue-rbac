import request from '@/utils/request'
import Qs from 'qs'

export function fetchList(query) {
    return request({
        url: '/article/list',
        method: 'get',
        params: query
    })
}

export function fetchArticle(id) {
    return request({
        url: '/article/detail',
        method: 'get',
        params: { id }
    })
}

export function fetchPv(pv) {
    return request({
        url: '/article/pv',
        method: 'get',
        params: { pv }
    })
}

export function createArticle(data) {
    const data1 = data
    return request({
        url: '/article/create',
        method: 'post',
        data: Qs.stringify(data1)
    })
}

export function deleteArticle(id) {
    return request({
        url: '/article/delete',
        method: 'delete',
        params: { id: id }
    })
}

export function updateArticle(data) {
    const data1 = data
    return request({
        url: '/article/update',
        method: 'post',
        data: Qs.stringify(data1)
    })
}

export function updateArticleStatus(id, status) {
    return request({
        url: '/article/updateArticleStatus',
        method: 'post',
        data: Qs.stringify({ id, status })
    })
}