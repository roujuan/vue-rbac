import request from '@/utils/request'
import Qs from 'qs'

export function getDepartmentList(query) {
    return request({
        url: '/department/list',
        method: 'get',
    })
}

export function fetchList(query) {
    return request({
        url: '/department/queryList',
        method: 'get',
        params: query
    })
}

export function deleteDepartment(id) {
    return request({
        url: '/department/delete',
        method: 'delete',
        params: { id: id }
    })
}

export function updateDepartment(data) {
    const data1 = data
    return request({
        url: '/department/update',
        method: 'post',
        data: Qs.stringify(data1)
    })
}

export function createDepartment(data) {
    const data1 = data
    return request({
        url: '/department/create',
        method: 'post',
        data: Qs.stringify(data1)
    })
}